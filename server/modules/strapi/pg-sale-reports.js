const logger = require('../logger');

const resource = 'pg-sale-reports';

const get = async (id) => {
  try {
    const client = require('../client')();
    const res = await client.get(`${resource}/${id}`);
    return res.data;
  } catch (error) {
    logger.error(error);
    return null
  }
};

module.exports = { get };