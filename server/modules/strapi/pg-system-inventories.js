const client = require('../client')();
const logger = require('../logger');

const resource = 'pg-system-inventories';

const get = async (prodId, outletId) => {
  try {
    const res = await client.get(`${resource}?pg_product=${prodId}&pg_outlet=${outletId}`);
    return res.data;
  } catch (error) {
    logger.error(error);
    return Promise.resolve([]);
  }
};

const update = async (id, quantity) => {
  try {
    const body = {
      "quantities": quantity
    };
    await client.put(`${resource}/${id}`, body);
  } catch (error) {
    logger.error(error);
    return Promise.resolve([]);
  }
};

const updateQuantityOrCreate = async ( productId, outletId, quantity ) => {
  try {
    let store = await get(productId, outletId)
    if(store.length > 0){
      await update(store[0].id, quantity)
    }else{
      await create(productId, outletId, quantity)
    }
  } catch (error) {
    logger.error(`update error obj: ${obj}`)
    return Promise.resolve([])
  }
}

const create = async (prodId, outletId, quantity) => {
  try {
    const body = {
      "pg_product": prodId,
      "pg_outlet": outletId,
      "quantities": quantity
    };
    await client.post(resource, body);
  } catch (error) {
    logger.error(error);
    return Promise.resolve([]);
  }
};

const getSysInventoriesWithProdId = async (id) => {
  try {
    let sysInventories = await client.get(`${resource}?pg_product.id=${id}`)
    return sysInventories.data
  } catch (error) {
    logger.error(`Get sysInventories error productId=${id}`, error)
    return []
  }
}

const deleteById = async (id) => {
  try {
    await client.delete(`${resource}/${id}`)
    logger.info(`Delete sysInventory id=${id}`)
  } catch (error) {
    logger.info(`Delete fail sysInventory id=${id}`)
    return
  }
}

module.exports = { get, update, create, updateQuantityOrCreate, getSysInventoriesWithProdId, deleteById };
