const pgSystemInventories = require('./pg-system-inventories');
const pgSaleReports = require('./pg-sale-reports');

module.exports = { pgSystemInventories, pgSaleReports };
