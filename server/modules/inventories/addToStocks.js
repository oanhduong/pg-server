const client = require('../client')()
const logger = require('../logger')
const { pgSystemInventories } = require('../strapi/index');

const updateProdQuantity = async (productId, quantities) => {
  try {
    const body = {
      "quantity": quantities
    }
    await client.put(`pg-products/${productId}`, body)
  } catch (error) {
    logger.error(`updateProdInventories error productId: ${productId}`);
    return Promise.resolve([])
  }
}

const multiple = async (req, res, next) => {
  try {
    let { body } = req
    body = Array.isArray(body) ? body:[body]
    await Promise.all(body.map(obj => pgSystemInventories.updateQuantityOrCreate(obj.productId, obj.outletId, obj.quantity)))
    let stores = await client.get(`pg-system-inventories?pg_product.id=${body[0].productId}`).then(rs => rs.data).catch(()=>[])
    let quantities = stores.map(e => e.quantities).reduce((a,b) => Number.parseInt(a) + Number.parseInt(b),0)
    await updateProdQuantity(body[0].productId, quantities)
    return res.status(204).json({message: 'Modify successfull'})
  } catch (error) {
    throw error
  }
}

module.exports = { multiple }