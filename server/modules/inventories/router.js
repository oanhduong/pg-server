const express = require('express')
const router = new express.Router()

router.put('/addToStocks', require('./addToStocks').multiple)

module.exports = router;
