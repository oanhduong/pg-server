const newInstance = () => {
    const axios = require('axios');
    const config = require('../../config');

    const options = {
        baseURL: config.strapi.endpoint,
        timeout: 5 * 60 * 1000,
        headers: {
            'Content-Type': 'application/json; charset=utf8',
            'Authorization': config.strapi.token,
        },
    };

    const client = axios.create(options);
    client.before = []
    client.after = []

    const runSeqBefore = async (config) => {
        if (client.before.length === 0) {
            return
        }
        const cb = client.before.shift()
        try {
            await cb(config)
        } catch (error) { }
        return runSeqBefore()
    }

    const runSeqAfter = async (response) => {
        if (client.after.length === 0) {
            return
        }
        const cb = client.after.shift()
        try {
            await cb(response)
        } catch (error) { }
        return runSeqAfter()
    }

    client.interceptors.request.use(async (config) => {
        client.before = client.before.filter((e) => e)
        await runSeqBefore(config)
        return config
    }, (error) => Promise.reject(error))

    client.interceptors.response.use(async (response) => {
        client.after = client.after.filter((e) => e)
        await runSeqAfter(response)
        return response
    }, (error) => Promise.reject(error))

    return client
}

module.exports = newInstance
