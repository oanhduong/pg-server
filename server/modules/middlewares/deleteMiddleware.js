const client = require('../client/index');
const logger = require('../logger');
const filterMiddleware = require('./filterMiddleware')
const deleteProd = require('./deleteProductMiddleware')

const deleteMiddleware = async (proxyRes, req, res) => {
  try {
    let path = req.path
    if(path.split('/')[1] === 'pg-sale-reports'){
      await filterMiddleware.checkAndDeleteFilter(proxyRes, req, res)
    }
    if(path.split('/')[1] === 'pg-products'){
      await deleteProd.deleteProductMiddleware(proxyRes, req, res)
    }
  } catch (error) {
    throw error
  }
}

module.exports = { deleteMiddleware }