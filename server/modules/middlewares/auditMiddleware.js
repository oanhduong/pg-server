const client = require('../client/index');

const url = ['pg-sale-reports', 'pg-inventories', 'pg-lot-dates'];

// const propToTable = {
//   'pg_product': 'pg-products',
//   'pg_outlet': 'pg-outlets',
//   'pg_user': 'pg-users'
// }

const method = ['PUT', 'DELETE'];

const createAudit = async (proxyRes, req, res) => {
  try {
    const {body, path} = req;
    let table = path.split('/')[1];
    let obj = {};
    if(method.includes(req.method) && url.includes(table)){
      if(req.method === 'PUT'){
        obj = await client.get(`${path}`).then(rs => rs.data).catch(err => console.log(err));
        const removeFields = ['created_by', 'updated_by', '__v', '_id', 'createdAt', 'updatedAt'];
        Object.keys(obj).forEach(el => {
        if(removeFields.includes(el) || Array.isArray(obj[el])) delete obj[el];
      });
      }
      let audit = {
        table: table,
        url: path,
        method: req.method,
        data: req.method === 'DELETE' ? '':JSON.stringify(obj),
        targetId: obj.id,
        byUser: req.user.name
      };
      await client.post('/pg-audits', audit);
    }
  } catch (error) {
    console.log(error);
  }
};

// const getDataById = async (prop, value) => {
//   if(propToTable[prop]){
//     let data =await client.get(`/${propToTable[prop]}/${value}`).then(rs => rs.data).catch(()=>'')
//     return data
//   }
//   return value
// }

module.exports = {createAudit};