const client = require('../client/index');
const logger = require('../logger');
const { pgSystemInventories } = require('../strapi/index');

const deleteProductMiddleware = async (proxyRes, req, res) => {
  try {
    let path = req.path
    let body = "";
    await proxyRes.on('data', (data) => {
      body += data.toString('utf-8');
    });
    body = JSON.parse(body)
    let sysInventories = body.pg_system_inventories
    await Promise.all(sysInventories.map(el => pgSystemInventories.deleteById(el.id)))
  } catch (error) {
    return
  }
}

module.exports = { deleteProductMiddleware }