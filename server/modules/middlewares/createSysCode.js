const create = (proxyReq, body, prefix) => {
  
  if (!body || !Object.keys(body).length) {
    return proxyReq;
  }

  const contentType = proxyReq.getHeader('Content-Type');
  let bodyData;

  if (contentType === 'application/json') {

    if (proxyReq.method === 'POST' && prefix) body.sys_code = genSysCode(prefix);

    bodyData = JSON.stringify(body);

  }

  if (bodyData) {

    proxyReq.setHeader('Content-Length', Buffer.byteLength(bodyData));
    proxyReq.write(bodyData); //re-write body

  }

  return proxyReq;
};

const genSysCode = (prefix) => {
  return ''.concat(prefix, formatCode());
};

const formatCode = () => {
  const timeNow = new Date();
  return ''.concat(
    timeNow.getFullYear(),
    ("0" + (timeNow.getMonth() + 1)).slice(-2),
    ("0" + (timeNow.getDate())).slice(-2),
    ("0" + (timeNow.getHours())).slice(-2),
    ("0" + (timeNow.getMinutes())).slice(-2),
    ("0" + (timeNow.getSeconds())).slice(-2),
    ("0" + (timeNow.getMilliseconds())).slice(-2)
  );
};

module.exports = { create };