const client = require('../client/index');
const logger = require('../logger');

table = {
  'sku': 'filter-sale-report-by-skuses',
  'outlet': 'filter-sale-report-by-outlets'
};

const checkAndDeleteFilter = async (proxyRes, req, res) => {
  try {
    let path = req.path;
    let body = "";
    await proxyRes.on('data', (data) => {
      body += data.toString('utf-8');
    });
    let {createdAt, pg_outlet, pg_product} = JSON.parse(body);
    await shouldDeleteFilter(path, createdAt, pg_outlet, pg_product);
  } catch (error) {
    logger.error('Delete filter get Error: ', error);
  }
};

const shouldDeleteFilter = async (path, createdAt, pg_outlet, pg_product) => {
  try {
    const deleleFilter = async (url, tab, time) => {
      try {
        let count = await client.get(url).then(rs => rs.data).catch(err => console.log(err));
        if(count === 0) {
          let {data} = await client.get(`${table[tab]}?date=${time.getTime()}&pg_outlet.id=${pg_outlet.id}${tab === 'sku' ? `&pg_product.id=${pg_product.id}`:''}`);
          await client.delete(`${table[tab]}/${data[0].id || 0}`);
          logger.info(`Delete ${table[tab]} ====> id: ${data[0].id}`);
        }
      } catch (error) {
        logger.error(`Error: Get ${table[tab]} `);
        throw err;
      }
    };
    let start = new Date(createdAt);
    start.setHours(0,0,0,0);
    let end = new Date(createdAt);
    end.setHours(0,0,0,0);
    end.setDate(end.getDate() + 1);
    
    //check and delete outlet filter
    let url = `${path.split('/')[1]}/count?createdAt_gte=${start.getTime()}&createdAt_lt=${end.getTime()}&pg_outlet.id=${pg_outlet.id}`;
    await deleleFilter(url, 'outlet', start);

    //check and delete sku filter
    url = url.concat(`&pg_product.id=${pg_product.id}`);
    await deleleFilter(url, 'sku', start);
    logger.info('Delete filter logs successfull');
  } catch (error) {
    throw error;
  }
};

const checkAndCreateFilter = async (proxyRes, req, res) => {
  try {
    let path = req.path
    if(path.split('/')[1] === 'pg-sale-reports' && req.method === 'POST'){
      logger.info('Check to create filter')
      let body = "";
      await proxyRes.on('data', (data) => {
        body += data.toString('utf-8');
      });
      let { createdAt, pg_outlet, pg_product } = JSON.parse(body)
      let time = new Date(createdAt)
      time.setHours(0,0,0,0)
      let filterBody = {
        date: time.getTime(),
        pg_outlet: pg_outlet.id
      }
      //sale report by outlet
      await shouldCreateFilter('outlet', filterBody)
      //sale report by sku
      filterBody.pg_product = pg_product.id
      await shouldCreateFilter('sku', filterBody)
    }
  } catch (error) {
    logger.error(`Error: Create filter ${table[filter]}`)
    return
  }
}

const shouldCreateFilter = async (filter, body) => {
  try {
    let { data } = await client.get(`${table[filter]}/count?date=${body.date}&&pg_outlet=${body.pg_outlet}${(filter === 'sku') ? `&&pg_product=${body.pg_product}`:''}`)
    if(data === 0) await client.post(`${table[filter]}`, body)
    logger.info(`count: ${data} ====> ${(data === 0) ? `Create filter sale report by ${filter}`:`Filter sale report by ${filter} already exist`}`);
  } catch (error) {
    logger.error(`Error: Create filter ${table[filter]}`)
    return
  }
}

module.exports = { checkAndDeleteFilter, checkAndCreateFilter };