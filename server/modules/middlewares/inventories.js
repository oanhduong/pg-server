const { pgSystemInventories, pgSaleReports } = require('../strapi/index');
const logger = require('../logger');

const doUpdate = async (saleRp, quantity) => {
  const sysInventories = await pgSystemInventories.get(saleRp.pg_product.id, saleRp.pg_outlet.id);

  if (sysInventories.length > 0) {
    await pgSystemInventories.update(sysInventories[0].id, Number(sysInventories[0].quantities) - quantity);
  } else {
    await pgSystemInventories.create(saleRp.pg_product.id, saleRp.pg_outlet.id, - quantity);
  }
}

const updateInv = async (req, res, next) => {
  req.afterStrapi = async (res) => { 
    if (res.status === 200) await doUpdate(res.data, res.config.quantities) 
  }
  next()
}

const remove = async (req, res, next) => {
  req.beforeStrapi = async (conf) => {
    const saleReport = await pgSaleReports.get(conf.url.split('/')[1])
    conf.quantities = Number(saleReport.itemCount) * -1
  }
  next()
}

const create = async (req, res, next) => {
  req.beforeStrapi = async (conf) => {
    conf.quantities = Number(conf.data.itemCount)
  }
  next()
}

const update = async (req, res, next) => {
  req.beforeStrapi = async (conf) => {
    const saleReport = await pgSaleReports.get(conf.url.split('/')[1])
    conf.quantities = Number(conf.data.itemCount) - Number(saleReport.itemCount)
  }
  next()
}

module.exports = { create, update, remove, updateInv };
