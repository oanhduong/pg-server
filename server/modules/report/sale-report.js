const client = require('../client')();
const dateFormat = require('dateformat');

const group = {
    outlet: 'filter-sale-report-by-outlets',
    sku: 'filter-sale-report-by-skuses'
};

const saleReport = async (req, res, next) => {
    try {
        let arr = await getSaleReportCriteria(req.query, req.params.groupBy);
        let rs = await groupAndSumData(arr.datas, true);

        let obj = {};
        obj.page = arr.page;
        obj.pagesize = arr.pagesize;
        obj.count = arr.count;
        obj.total = arr.total;
        obj.arr = rs;

        return res.status(200).json(obj);
    } catch (error) {
        next(error);
    }
};

const getSaleReportCriteria = async (filt, groupBy) => {
    const {distributor, city, outlet, fromdate, todate, page = 1, pagesize = 5} = filt;
    let url = group[groupBy]; 
    if(!url){
        throw new Error(`groupBy [${groupBy}] invalid`);
    }
    let filter = '?_sort=date:DESC';
    if(distributor){
        filter = filter.concat(`&pg_outlet.group=${encodeURIComponent(distributor)}`);
    }
    if(city){
        filter = filter.concat(`&pg_outlet.city=${encodeURIComponent(city)}`);
    }
    if(outlet){
        filter = filter.concat(`&pg_outlet.name=${encodeURIComponent(outlet)}`);
    }
    if(fromdate){
        let date = new Date(fromdate);
        filter = filter.concat(`&date_gte=${date.getTime()}`);
    }
    if(todate){
        let date = new Date(todate);
        filter = filter.concat(`&date_lte=${date.getTime()}`);
    }
    let obj = {};
    //count data
    let allRecord = await client.get(url.concat(filter)).then(rs => rs.data).catch(() => 0);
    obj.count = allRecord.length;
    let sumWithCri = await groupAndSumData(allRecord, false);
    obj.total = {
        reach: sumWithCri.map(e => e.reach).reduce((a,b) => Number.parseInt(a) + Number.parseInt(b), 0),
        display: groupBy === 'sku' ? sumWithCri.map(e => e.display).reduce((a,b) => Number.parseInt(a) + Number.parseInt(b), 0) : '',
        buyer: sumWithCri.map(e => e.buyer).reduce((a,b) => Number.parseInt(a) + Number.parseInt(b), 0),
        actual: sumWithCri.map(e => e.actual).reduce((a,b) => Number.parseInt(a) + Number.parseInt(b), 0)
    };
    obj.page = page;
    obj.pagesize = pagesize;
    //add page and pagesize
    url = url.concat(filter).concat(`&_limit=${pagesize}&_start=${(page - 1) * pagesize}`);
    //get data with page and pagesize
    obj.datas = await client.get(url).then(rs => rs.data).catch(() => []);

    return obj;
};

const groupAndSumData = async (arr, showDetail) => {
    let data = await Promise.all(arr.map( async (el) => {
        let start = new Date(el.date);
        let end = new Date(el.date);
        end.setDate(end.getDate() + 1);
        let url = `/pg-sale-reports?pg_outlet.id=${el.pg_outlet.id}&createdAt_gte=${start.getTime()}&createdAt_lt=${end.getTime()}`;
        if(el.pg_product){
            url = url.concat(`&pg_product.id=${el.pg_product.id}`);
        }
        let rs = await client.get(url).then(rs => rs.data).catch(()=> []);
        if(rs.length === 0){
            return null;
        }
        let salesTarget = rs[0]['pg_outlet']['target'];
        let actual = rs.map(e => e.itemCount * e.pg_product.price).reduce((a,b) => a + b, 0);

        return {
            filterId: el.id,
            date: dateFormat(el.date, 'dd/mm/yyyy'),
            distributor: rs[0]['pg_outlet']['group'],
            outlet: rs[0]['pg_outlet']['name'],
            sku: el.pg_product ? el.pg_product.name : '',
            reach: rs.map(e => e.reach).reduce((a,b) => a + Number.parseInt(b), 0),
            display: el.pg_product ? rs.map(e => e.actualDisplay).reduce((a,b) => a + Number.parseInt(b), 0) : '',
            buyer: rs.map(e => e.buyers).reduce((a,b) => a + Number.parseInt(b), 0),
            salesTarget: salesTarget,
            actual: actual,
            targetPercent: (actual/salesTarget)*100,
            detail: showDetail ? rs : ''
        };
    }));

    return data.filter(e =>e);
};

module.exports = {saleReport};