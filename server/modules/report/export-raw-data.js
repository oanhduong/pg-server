const client = require('../client')();
const logger = require('../logger');
const xlsx = require('xlsx');
const camelcaseKeys = require('camelcase-keys');
const dateFormat = require('dateformat');
const camelcase = require('camelcase');

const rawData = async (req, res, next) => {
    try {
        const custom = (obj, lever) => {
            const changeProp = (prop) => {
                if(obj[prop] !== null && typeof obj[prop] === 'object'){
                    let newProp = prop;
                    switch (lever) {
                        case 0:
                            newProp = newProp.replace('pg_', '');
                            obj[newProp] = obj[prop].sys_code || obj[prop].id;
                            delete obj[prop];
                            break;
                        case 1:
                            let prefix = newProp.replace('pg_', '');
                            let arrPropLv1 = Object.keys(obj[prop]);
                            arrPropLv1.forEach(el => (listRemove.includes(el) || Array.isArray(obj[prop][el])) ? delete obj[prop][el] : obj[prefix.concat('_').concat(el.replace('pg_',''))] = listFormat.includes(el) ? dateFormat(new Date(obj[prop][el]), 'mm/dd/yyyy HH:MM') : obj[prop][el]);
                            delete obj[prop];
                            break;
                    }
                } else if(listFormat.includes(prop)) obj[prop] = dateFormat(new Date(obj[prop]), 'mm/dd/yyyy HH:MM');
            };
            let listRemove = ['__v', '_id', 'image', 'createdAt', 'updatedAt', 'updated_by', 'created_by', 'password'];
            let listFormat = ['startDate', 'endDate', 'reportDate', 'expiredDate'];
            let arrProp = Object.keys(obj);
            arrProp.forEach(prop => (listRemove.includes(prop) || Array.isArray(obj[prop])) ? delete obj[prop] : changeProp(prop));

            return camelcaseKeys(obj, {pascalCase: true});
        };

        const singleHandle = async (mod) => {
            let objects = await client.get(mod.table).then(result => result.data).catch(()=>[]);
            objects = objects.map(obj => custom(obj, mod.lever));

            return {table: mod.table, arrs: objects};

        };

        let arrTable = [{table:'pg-users', lever: 1}, {table: 'pg-checkins', lever: 1}, {table: 'pg-outlets', lever: 1}, {table: 'pg-products', lever: 1}, {table: 'pg-product-groups', lever: 1}, {table: 'pg-working-sheets', lever: 1}, {table: 'pg-sale-reports', lever: 1}, {table: 'pg-inventories', lever: 1}, {table: 'pg-lot-dates', lever: 1}, {table: 'pg-competitors', lever: 1}, {table: 'pg-gifts', lever: 1}];
        let datas = await Promise.all(arrTable.map(singleHandle));
        let wss = datas.map(createWorkSheets);
        let date = new Date();
        let linkFile = exportData(wss, `rawData_${date.getTime()}`);

        res.status(200).json({link: linkFile});

    } catch (error) {
        next(error);
    }

};

const createWorkSheets = (datas) => {

    let {table, arrs} = datas;
    table = camelcase(table.replace('pg-', ''), {pascalCase: true});
    arrs = Array.isArray(arrs[0]) ? arrs : [arrs];
    let ws;
    let count = 0;
    arrs.forEach(el => {
        if(!ws){
            ws = xlsx.utils.json_to_sheet(el);
        }else{
            xlsx.utils.sheet_add_json(ws, el, {origin: { r: count, c: 0 }});
        }
        count = count + el.length + 3;
    });

    return {sheetName: table, ws: ws};

};

const exportData = (wss, fileName) => {

    wss = Array.isArray(wss) ? wss : [wss];
    let wb = xlsx.utils.book_new();
    wss.forEach(ws => xlsx.utils.book_append_sheet(wb, ws.ws, ws.sheetName));
    xlsx.writeFile(wb, `./static/export/${fileName}.xlsx`);

    return `/static/export/${fileName}.xlsx`;

};

module.exports = {rawData};