const client = require('../client')();

const mandatory = (parameter = 'Parameter') => {
  throw new Error(`${parameter} is required`);
};

const audit = async (req, res, next) => {
  try {
    let filter = '_sort=createdAt:DESC';
    let {fromDate , toDate } = req;
    if(fromDate){
      let start = new Date(fromDate);
      start.setHours(0,0,0,0);
      filter.concat(`&createdAt_gte=${start.getTime()}`);
    }
    if(toDate){
      let end = new Date(toDate);
      end.setHours(0,0,0,0);
      end.setDate(end.getDate() + 1);
      filter.concat(`&createdAt_lt=${end.getTime()}`);
    }
    let start = new Date(fromDate);
    start.setHours(0,0,0,0);
    let end = new Date(toDate);
    end.setHours(0,0,0,0);
    end.setDate(end.getDate() + 1);
    let hist = await client.get(`pg-audits?${filter}`).then(rs => rs.data).catch(() => []);

  } catch (error) {
    next(error);
  }
};