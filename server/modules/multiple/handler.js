const logger = require('../logger');
const newClient = (req) => {
    const client = require('../client')()
    client.before.push(req.beforeStrapi)
    client.after.push(req.afterStrapi)
    return client
}

const create = (resource) => async (req, res, next) => {
    
    const createOne = async (data) => {
        const client = newClient(req)
        try {
            const res = await client.post(resource, data);
            return res.data;
        } catch (error) {
            logger.error(`Create ${resource} error`, error);
            return null;
        }
    };
    try {
        const data = Array.isArray(req.body) ? req.body : [req.body];
        let newData = await Promise.all(data.map(createOne));
        newData = newData.filter((u) => u);
        res.status(200).json(newData);
    } catch (error) {
        next(error);
    }
};

const update = (resource) => async (req, res, next) => {

    const updateOne = async (data) => {
        const client = newClient(req)
        try {
            const _data = JSON.parse(JSON.stringify(data))
            delete data.id
            const res = await client.put(`${resource}/${_data.id}`, data);
            return res.data;
        } catch (error) {
            logger.error(`Update ${resource} error`, error);
            return null;
        }
    };
    try {
        const data = Array.isArray(req.body) ? req.body : [req.body];
        let newData = await Promise.all(data.map(updateOne));
        newData = newData.filter((u) => u);
        res.status(200).json(newData);
    } catch (error) {
        next(error);
    }
};

const remove = (resource) => async (req, res, next) => {

    const removeOne = async (id) => {
        const client = newClient(req)
        try {
            const res = await client.delete(`${resource}/${id}`);
            return res.data;
        } catch (error) {
            logger.error(`Delete ${resource} error`, error);
            return null;
        }
    };
    try {
        const data = Array.isArray(req.body) ? req.body : [req.body];
        let newData = await Promise.all(data.map(removeOne));
        newData = newData.filter((u) => u);
        res.status(200).json(newData);
    } catch (error) {
        next(error);
    }
};

module.exports = { create, update, remove };
