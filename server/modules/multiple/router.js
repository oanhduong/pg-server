const express = require('express');
const router = new express.Router();
const handler = require('./handler');
const inventories = require('../middlewares/inventories');

router.post('/checkins', handler.create('pg-checkins'));
router.post('/outlets', handler.create('pg-outlets'));
router.post('/products', handler.create('pg-products'));
router.post('/product-groups', handler.create('pg-product-groups'));
router.post('/working-sheets', handler.create('pg-working-sheets'));
router.post('/sale-reports', inventories.create, inventories.updateInv, handler.create('pg-sale-reports'));
router.post('/inventories', handler.create('pg-inventories'));
router.post('/lot-dates', handler.create('pg-lot-dates'));
router.post('/competitors', handler.create('pg-competitors'));
router.post('/gifts', handler.create('pg-gifts'));
router.post('/system-inventories', handler.create('pg-system-inventories'));

router.put('/sale-reports', inventories.update, inventories.updateInv, handler.update('pg-sale-reports'));

router.delete('/sale-reports', inventories.remove, inventories.updateInv, handler.remove('pg-sale-reports'));

module.exports = router;
