const client = require('../client')();

const getAllOutlets = async () => {
  try {
    const res = await client.get('/pg-outlets');
    return res.data.map(e => e.id);
  } catch (error) {
    console.error(error);
  }
};

const getAllProducts = async () => {
  try {
    const res = await client.get('/pg-products');
    return res.data.map(e => e.id);
  } catch (error) {
    console.error(error);
  }
};

const run = async () => {
  try {
    const result = await Promise.all([getAllOutlets(), getAllProducts()]);
    const list = result[0].map(outlet => {
      return result[1].map(product => ({
        "pg_product": product,
        "pg_outlet": outlet,
        "quantities": Math.floor(Math.random() * 1000) + 1
      }));
    });
    await Promise.all([].concat.apply([],list).map(e => client.post('/pg-system-inventories', e)));
  } catch (error) {
    console.error(error);
  }
};

run();