global.BASE_DIR = __dirname;

const http = require('http');
const express = require('express');
const config = require('./config');
const logger = require('./modules/logger');
const cors = require('cors');
const fileUpload = require('express-fileupload');
const { createProxyMiddleware } = require('http-proxy-middleware');
const path = require('path');

const app = express();
app.use(express.static(path.join(__dirname, 'public')));
app.use(cors());
app.use('/api/v1', express.json());
app.use('/api/v2', express.json());
app.use('/api/v3', express.json());
app.use(express.urlencoded({ extended: false }));
app.use('/api/v1', fileUpload());

app.use('/api/v1/login', require('./modules/auth').login);
app.use('/api', require('./modules/auth').verify);

app.post('/api/v1/users', require('./modules/user').createUserMidWare);
app.use('/api/v1/csv', require('./modules/csv').router);
app.use('/api/v1/reports', require('./modules/report').router);
app.use('/api/v1/inventories', require('./modules/inventories').router);

app.use(['/api/v2', '/uploads'], createProxyMiddleware({
  target: config.strapi.endpoint,
  changeOrigin: true,
  prependPath: false,
  pathRewrite: {
    '^/api/v2/users': '/pg-users',
    '^/api/v2/checkins': '/pg-checkins',
    '^/api/v2/outlets': '/pg-outlets',
    '^/api/v2/products': '/pg-products',
    '^/api/v2/product-groups': '/pg-product-groups',
    '^/api/v2/working-sheets': '/pg-working-sheets',
    '^/api/v2/sale-reports': '/pg-sale-reports',
    '^/api/v2/inventories': '/pg-inventories',
    '^/api/v2/lot-dates': '/pg-lot-dates',
    '^/api/v2/competitors': '/pg-competitors',
    '^/api/v2/gifts': '/pg-gifts',
    '^/api/v2/upload': '/upload',
    '^/api/v2/filter-sale-report-by-outlets': '/filter-sale-report-by-outlets',
    '^/api/v2/filter-sale-report-by-skuses': '/filter-sale-report-by-skuses',
    '^/api/v2/audit': '/pg-audits',
    '^/api/v2/system-inventories': '/pg-system-inventories',
    '^/api/v2/promotions': '/pg-promotions',
    '^/uploads': '/uploads',
  },
  onProxyReq: (proxyReq, req, res) => {
    proxyReq.setHeader('Authorization', config.strapi.token);

    arrPaths = [
      {
        path: '/pg-products',
        prefix: 'PROD_'
      },
      {
        path: '/pg-users',
        prefix: 'USER_'
      },
      {
        path: '/pg-outlets',
        prefix: 'OUTL_'
      },
    ];

    selectedPath = arrPaths.find(e => req.path === e.path) || { prefix: null };

    proxyReq = require('./modules/middlewares/createSysCode').create(proxyReq, req.body, selectedPath.prefix);

  },
  onProxyRes: (proxyRes, req, res) => {
    require('./modules/middlewares/auditMiddleware').createAudit(proxyRes, req, res)
    require('./modules/middlewares/filterMiddleware').checkAndCreateFilter(proxyRes, req, res)
    if(req.method === 'DELETE'){
      require('./modules/middlewares/deleteMiddleware').deleteMiddleware(proxyRes, req, res)
    }
  }
}));

app.use('/api/v3', require('./modules/multiple').router);

app.get('*', (req, res) => res.sendFile(path.join(__dirname, 'public', 'index.html')));

app.use((req, res) => {
  res.status(404).json({ message: `CANNOT ${req.method} API ${req.originalUrl}` });
});

app.use((err, req, res, _next) => res.status(500).json({ message: err.message || String(err) }));

const server = http.createServer(app);
server.listen(config.port.http, (err) => {
  if (err) {
    logger.error(`Start server error`, config, err);
  } else {
    logger.info(`Server is listening on port ${config.port.http}`);
  }
});
