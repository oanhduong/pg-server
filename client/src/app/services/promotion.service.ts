import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { forkJoin, Observable } from 'rxjs';
import { BaseService } from './base.service';
import * as queryString from 'query-string';

@Injectable({
  providedIn: 'root'
})

  
  
export class PromotionService extends BaseService {
  
  constructor(private http: HttpClient) {
    super();
  }

  fetch(filter?: any): Observable<any[]> {
    let query = '';
    if (filter) {
      query = queryString.stringify(this.generateFilter(filter));
    }
    return this.http.get<any[]>(`/api/v2/promotions?isDelete=false${query ? '&' : ''}${query}`);
  }

  getTotalRecords(filter?: any): Observable<number> {
    let query = '';
    if (filter) {
      delete filter.pageSize
      delete filter.page
      query = queryString.stringify(this.generateFilter(filter));
    }
    return this.http.get<number>(`/api/v2/promotions/count?isDelete=false${query ? '&' : ''}${query}`);
  }

  create(data): Observable<any[]> {
    return this.http.post<any[]>('/api/v2/promotions', data);
  }

  get(id): Observable<any> {
    return this.http.get<any>(`/api/v2/promotions/${id}`);
  }

  update(id: number, data: any): Observable<any[]> {
    return this.http.put<any[]>(`/api/v2/promotions/${id}`, data);
  }

  delete(id: number): Observable<any[]> {
    return this.http.delete<any[]>(`/api/v2/promotions/${id}`);
  }

  fetchPromotionsOverlapByDate(start, end): Observable<any[]> {
    return forkJoin([
      this.http.get<any[]>(`/api/v2/promotions?isDelete=false&start_gte=${start}&start_lte=${end}`),
      this.http.get<any[]>(`/api/v2/promotions?isDelete=false&end_lt=${end}&end_gte=${start}`),
      this.http.get<any[]>(`/api/v2/promotions?isDelete=false&start_lte=${start}&end_gte=${end}`),
    ])
  }



}
