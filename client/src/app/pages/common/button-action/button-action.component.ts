import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-button-action',
  templateUrl: './button-action.component.html'
})
export class ButtonActionComponent implements OnInit {
    @Input() exporting = false;
    @Input() disableExporting = false;
    
    @Output() onAdd: EventEmitter<any> = new EventEmitter<any>();
    @Output() onDownloadTemplate: EventEmitter<any> = new EventEmitter<any>();
    @Output() onImport: EventEmitter<any> = new EventEmitter<any>();
    @Output() onExport: EventEmitter<any> = new EventEmitter<any>();

    ngOnInit() {
    }

    onFileSelected(event) {
      console.log('Event >>>>>>>', event);
    }
}
