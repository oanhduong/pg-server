import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { CategoryService } from 'src/app/services/category.service';
@Component({
  selector: 'app-categories-create-dialog',
  templateUrl: './categories-create-dialog.component.html',
  styleUrls: ['./categories-create-dialog.component.scss'],
})
export class CategoriesCreateDialogComponent implements OnInit {
  pgManagementForm: FormGroup;
  title = '';
  buttonTitle = '';
  loading = false;
  constructor(@Inject(MAT_DIALOG_DATA) public data: any, private fb: FormBuilder,
  private categoryService: CategoryService,
  public dialogRef: MatDialogRef<CategoriesCreateDialogComponent>) {
    this.title = data.type === 'create' ? 'Create Category' : 'Update Category';
    this.buttonTitle = data.type === 'create' ? 'Create' : 'Update';
    // this.title = data.title;
    this.pgManagementForm = this.fb.group({
      name: [null, Validators.required]
    });
  }
  ngOnInit(): void {
    if (this.data.type === 'update') {
      this.pgManagementForm.controls.name.setValue(this.data.data.name);
    }
  }

  createPg() {
    if (this.pgManagementForm.valid) {
      this.loading = true;
      if (this.data.type === 'create') {

        this.categoryService.create(this.pgManagementForm.value).subscribe(res => {
          this.loading = false;
          this.dialogRef.close(res);
        });
      }
      if (this.data.type === 'update') {
        this.categoryService.update(this.data.data.id, this.pgManagementForm.value).subscribe(res => {
          this.loading = false;
          this.dialogRef.close(res);
        });
      }
    }
  }
}
