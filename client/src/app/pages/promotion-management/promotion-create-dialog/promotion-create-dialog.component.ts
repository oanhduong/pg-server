import { Component, ElementRef, Inject, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { CategoryService } from 'src/app/services/category.service';
import { ProductService } from 'src/app/services/product.service';
import {COMMA, ENTER} from '@angular/cdk/keycodes';
import { Observable, Subscription } from 'rxjs';
import {MatAutocompleteSelectedEvent, MatAutocomplete} from '@angular/material/autocomplete';
import { map, startWith } from 'rxjs/operators';
import { MatChipInputEvent } from '@angular/material/chips';
import { PromotionService } from 'src/app/services/promotion.service';
import { ToastrService } from 'ngx-toastr';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';

@Component({
  selector: 'app-promotion-create-dialog',
  templateUrl: './promotion-create-dialog.component.html',
  styleUrls: ['./promotion-create-dialog.component.scss'],
})
export class PromotionCreateDialogComponent implements OnInit {
  pgManagementForm: FormGroup;
  title = '';
  buttonTitle = '';
  loading = false;
  products: any[] = [];
  isClosed = false;
  productSelect: any = null;

  visible = true;
  selectable = true;
  removable = true;
  separatorKeysCodes: number[] = [ENTER, COMMA];
  fruitCtrl = new FormControl();
  filteredFruits: any[] = [];
  fruits: any[] = [];
  allFruits: any[] = [];
  fetchPromotionOverlapped: Subscription;
  allProductInital: any[] = [];
  displayedColumns: string[] = ['code', 'name', 'action'];
  dataSource = new MatTableDataSource([]);
  
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('productInput') productInput: ElementRef<HTMLInputElement>;
  @ViewChild('auto') matAutocomplete: MatAutocomplete;
  
  promotionTypes = [
    {
      value: 'discount',
      label: 'Discount'
    },
    {
      value: 'Buy1get1free',
      label: 'Buy 1 Get 1 Free'
    }
  ]
  constructor(@Inject(MAT_DIALOG_DATA) public data: any, private fb: FormBuilder,
  private categoryService: CategoryService,
  private productService: ProductService,
  private promotionService: PromotionService,
  private toastr: ToastrService,
  public dialogRef: MatDialogRef<PromotionCreateDialogComponent>) {
    this.title = data.type === 'create' ? 'Create Promotion' : 'Update Promotion';
    this.buttonTitle = data.type === 'create' ? 'Create' : 'Update';
    this.pgManagementForm = this.fb.group({
      name: [null, Validators.required],
      start: [null, Validators.required],
      end: [null, []],
      discount: [null, []]
    });

  }
  ngOnInit(): void {
    if (this.data.type === 'update') {
      this.fruits = this.data.data.pg_products;
      this.dataSource = new MatTableDataSource(this.fruits);
      this.dataSource.sort = this.sort;
      this.changePromotionType();
    }
    this.productService.fetch().subscribe((rs) => {
      this.allProductInital = JSON.parse(JSON.stringify(rs));
      this.allFruits = JSON.parse(JSON.stringify(rs));
      this.filterFruiltFunction();
    });
  }

  filterFruiltFunction() {
    const productsSelected = this.fruits.map(({id}) => id);
    this.filteredFruits = this.allFruits.filter((product) => {
      return !productsSelected.includes(product.id);
    })
  }

  createPg() {
    if (this.pgManagementForm.valid) {
      const {start, end} = this.data.data;
      if (new Date(start).getTime() > new Date(end).getTime()) {
        this.toastr.error('', 'Start Date is greater than end date!');
        return;
      }
      const promotionData = JSON.parse(JSON.stringify(this.data.data));
      if (promotionData.promotionType === 'Buy1get1free') {
        promotionData.discount = null;
      }
      const startDateObject = new Date(promotionData.start);
      const endDateObject = new Date(promotionData.end);
      promotionData.start = new Date(Date.UTC(startDateObject.getFullYear(), startDateObject.getMonth(), startDateObject.getDate(),0,0,0));
      promotionData.end = new Date(Date.UTC(endDateObject.getFullYear(), endDateObject.getMonth(), endDateObject.getDate(),0,0,0));
      promotionData.pg_products = (this.fruits || []).map(({id}) => id);
      if (this.data.type === 'create') {
        this.promotionService.create(promotionData).subscribe(() => {
          this.toastr.success('', 'Create Promotion successfully!');
          this.dialogRef.close(true);
        }, _ => {
          this.toastr.error('', 'Faled to create Promotion!')
        })
      } else {
        this.promotionService.update(this.data.data.id, promotionData).subscribe(() => {
          this.toastr.success('', 'Update Promotion successfully!');
          this.dialogRef.close(true);
        }, _ => {
          this.toastr.error('', 'Failed to update Promotion!');
        })
      }
    }
  }

  changePromotionType() {
    if (this.data.data.promotionType === 'Buy1get1free') {
      this.pgManagementForm.controls.discount.setValidators(null);
    } else {
      this.pgManagementForm.controls.discount.setValidators([Validators.required, Validators.pattern('\\d+'), Validators.max(100)]);
    }
    this.pgManagementForm.controls.discount.updateValueAndValidity();
  }

  fetchCategories() {
    this.loading = true
    this.categoryService.fetch().subscribe((rs) => {
      // this.categories = rs;
      this.loading = false
    }, _ => this.loading = false);
  }

  add(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;

    // Add our fruit
    if ((value || '').trim()) {
      this.fruits.push(value.trim());
    }

    // Reset the input value
    if (input) {
      input.value = '';
    }
    this.dataSource = new MatTableDataSource(this.fruits);
    this.dataSource.sort = this.sort;
    this.fruitCtrl.setValue(null);
  }

  remove(fruit: string): void {
    const index = this.fruits.indexOf(fruit);

    if (index >= 0) {
      this.fruits.splice(index, 1);
    }
    this.dataSource = new MatTableDataSource(this.fruits);
    this.dataSource.sort = this.sort;
    this.filterFruiltFunction();
  }

  selected(event: MatAutocompleteSelectedEvent): void {
    // this.fruits.push(event.option.value);
    // this.fruitInput.nativeElement.value = '';
    // this.fruitCtrl.setValue(null);
    // this.filterFruiltFunction();

  }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.allFruits.filter(fruit => fruit.toLowerCase().indexOf(filterValue) === 0);
  }

  filterProductSelected(event) {
    const productsSelected = this.fruits.map(({id}) => id);
    this.filteredFruits = this.allFruits.filter((product) => {
      return !productsSelected.includes(product.id) && (event.data ? product.name.toLowerCase().includes(event.data.toLowerCase()) : true);
    })
  }

  selectProduct(value) {
    this.productSelect = value.option.value;
  }

  addProduct() {
    this.fruits.push(this.productSelect);
    this.dataSource = new MatTableDataSource(this.fruits);
    this.dataSource.sort = this.sort;
    this.filterFruiltFunction();
    this.productSelect = null;
    this.productInput.nativeElement.value = '';
  }

  dateChanged() {
    if (this.pgManagementForm.controls.start.valid && this.pgManagementForm.controls.end.valid) {
      let {start, end} = this.data.data;
      if (!start || !end) return
      const startDateObject = new Date(start);
      const endDateObject = new Date(end);
      if (startDateObject.getTime() < endDateObject.getTime()) {
        const offset = -(new Date().getTimezoneOffset()/60);
        if (this.fetchPromotionOverlapped) this.fetchPromotionOverlapped.unsubscribe();
        if (!start._i) {
          start= {
            _i: {
              year: startDateObject.getFullYear(),
              month: startDateObject.getMonth(),
              date: startDateObject.getDate()
            }
          }
        }
        if (!end._i) {
          end = {
            _i: {
              year: endDateObject.getFullYear(),
              month: endDateObject.getMonth(),
              date: endDateObject.getDate()
            }
          }
        }
        const startTime = new Date(Date.UTC(start._i.year, start._i.month, start._i.date,0,0,0)).getTime() - (offset * 60*60*1000);
        const endTime = new Date(Date.UTC(end._i.year, end._i.month, end._i.date + 1,0,0,0)).getTime() - (offset * 60*60*1000);
        this.fetchPromotionOverlapped = this.promotionService.fetchPromotionsOverlapByDate(startTime, endTime)
          .subscribe((promotions) => {
            this.toastr.warning('', 'Selected products may be deleted because they may already exist in other Promotions')
            if (this.data.data.id) {
              promotions = promotions.filter(({id}) => id !== this.data.data.id);
            }
            const productsOverlapped = promotions.reduce((arrayProducts, promotion) => {
              let products = [];

              promotion.map(({pg_products}) => {
                products = products.concat(pg_products)
              })
              return arrayProducts.concat(products);
            }, [])
            .map((product) => product.id);
            this.allFruits = this.allProductInital.filter(({id}) => !productsOverlapped.includes(id));
            this.fruits = this.fruits.filter(({id}) => !productsOverlapped.includes(id));
            this.dataSource = new MatTableDataSource(this.fruits);
            this.dataSource.sort = this.sort;
            if (this.productSelect && productsOverlapped.includes(this.productSelect.id)) {
              this.productSelect = null;
              this.productInput.nativeElement.value = '';
            }
            this.filterFruiltFunction();
          });
      }
    }
  }

}
