import {
  AfterViewInit,
  Component, OnDestroy, OnInit, ViewChild,
} from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { DeleteDialogComponent } from '../common/delete-dialog/delete-dialog.component';
import { ExportService } from 'src/app/services/export.service';
import { OutletCreateDialogComponent } from './outlet-create-dialog/outlet-create-dialog.component';
import { OutletService } from 'src/app/services/outlet.service';
import { forkJoin, Subscription } from 'rxjs';
import { Cities, Regions } from 'src/app/constant/constant';
import { CurrencyService } from '../common/currency-pipe/currency-pipe.service';
import { AddressDialogComponent } from './checkin-address-dialog/checkin-address-dialog.component';

@Component({
  selector: 'app-outlet-management',
  templateUrl: './outlet-management.component.html',
  styleUrls: ['./outlet-management.component.scss'],
})
export class OutletManagementComponent implements OnInit, AfterViewInit, OnDestroy{
  @ViewChild(MatSort) sort: MatSort;

  eventSubscription: Subscription;
  totalCount = 0;

  pageSizeOptions = [5, 10, 25, 100];
  displayedColumns: string[] = ['name', 'group', 'address', 'ward', 'district', 'city', 'region', 'target', 'location', 'action'];
  displayedSearchColumns: string[] = ['nameSearch', 'groupSearch', 'addressSearch', 'citySearch', 'regionSearch', 'targetSearch',  'locationSearch', 'actionSearch'];
  dataSource = new MatTableDataSource([]);
  listGroups = [];
  cities: any[] = [];
  loading = false;
  filter: any = {
    page: 0,
    pageSize: this.pageSizeOptions[0],
    name_contains: null,
    address_contains: null,
    group_contains: null,
    city_contains: null,
    region_contains: null,
    target: null
  };

  exporting = false;
  searchTimeout;

  ngOnInit() {
    this.listGroups = Regions;
    this.cities = Cities;
    this.initOutlets();
  }
  ngAfterViewInit() {
    this.dataSource.sort = this.sort;
  }

  initOutlets() {
    this.loading = true;
    this.eventSubscription = forkJoin([this.outletserive.fetchoutlets(this.filter), this.outletserive.getTotalRecords(this.filter)])
      .subscribe(([outlet, total]) => {
        this.dataSource = new MatTableDataSource(outlet);
        this.dataSource.sort = this.sort;
        this.totalCount = total;
        this.loading = false;
      }, _ => this.loading = false);
  }

  constructor(public dialog: MatDialog, private exportService: ExportService, private outletserive: OutletService,
    private currencyService: CurrencyService) { }

  ngOnDestroy(): void {
    if (this.eventSubscription) {
      this.eventSubscription.unsubscribe();
    }
  }
  createPg() {
    const dialogRef = this.dialog.open(OutletCreateDialogComponent, {
      width: '50%',
      data: { type: 'create' }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.filter.page = 0;
        this.filter.pageSize = this.pageSizeOptions[0];
        this.initOutlets();
      }
    });
  }

  modifyPg(element) {
    const dialogRef = this.dialog.open(OutletCreateDialogComponent, {
      width: '50%',
      data: {
        type: 'update',
        data: element
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.filter.page = 0;
        this.filter.pageSize = this.pageSizeOptions[0];
        this.initOutlets();
      }
    });
  }

  deletePg(id) {
    const dialogRef = this.dialog.open(DeleteDialogComponent, {
      data: {
        header: 'Delete Location',
        title: 'Do you want to delete this location?'
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.outletserive.deleteoutlet(id).subscribe(res => {
          this.filter.page = 0;
          this.filter.pageSize = this.pageSizeOptions[0];
          this.initOutlets();
        });
      }
    });
  }

  export() {
    this.exporting = true;
    const fileName = 'Outlet';
    const header: any = {
      name: 'Name',
      group: 'Customer',
      address: 'Address',
      ward: 'Ward',
      district: 'District',
      city: 'City',
      region: 'Region',
      target: 'Target',
      location: 'Location'
    }
    let data = [header];
      this.exporting = true;
      let newFilter = {...this.filter};
      delete newFilter.page;
      delete newFilter.pageSize;
      this.outletserive.fetchoutlets(newFilter)
        .subscribe((rs: any[]) => {
          let dataFilter = (rs || []).map(element => ({
            name: element.name,
            address: element.address,
            ward: element.ward,
            district: element.district,
            location: element.location ? `https://maps.google.com/maps?q=${element.location.replace('|', ',')}`: '',
            target: this.currencyService.transform(element.target, true),
            group: element.group,
            city: element.city,
            region: element.region

          }));
        data = data.concat(dataFilter);
        this.exportService.exportExcel(data, fileName, true, [fileName]);
        this.exporting = false;
      }, _ => {
        this.exporting = false;
      })
  }

  onPageChange({ pageIndex, pageSize }) {
    this.filter.page = pageIndex;
    this.filter.pageSize = pageSize;
    this.initOutlets();
  }

  openMap(address) {
    const dialogRef = this.dialog.open(AddressDialogComponent, {
      data: { address: `https://maps.google.com/maps?q=${address.replace('|', ',')}`}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }
  

  changeSearch(event) {
    this.filter.name_contains = event;
    if (this.searchTimeout) {
      clearTimeout(this.searchTimeout);
    }
    this.searchTimeout = setTimeout(() => this.initOutlets(), 200)
  }

  clearFilter() {
    for (const propName in this.filter) {
      if (!['page', 'pageSize'].includes(propName)) {
        this.filter[propName] = null;
      }
    }
    this.initOutlets();
  }
}
