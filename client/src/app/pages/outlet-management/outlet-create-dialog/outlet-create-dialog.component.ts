import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Cities, Groups, Regions } from 'src/app/constant/constant';
import { OutletService } from 'src/app/services/outlet.service';
@Component({
  selector: 'app-outlet-management',
  templateUrl: './outlet-create-dialog.component.html',
  styleUrls: ['./outlet-create-dialog.component.scss'],
})
export class OutletCreateDialogComponent implements OnInit{
  pgManagementForm: FormGroup;
  regions: any[] = [];
  cities: any[] = [];
  groups: any[] = [];
  title: string = '';

  loading = false;

  constructor(
    public dialogRef: MatDialogRef<OutletCreateDialogComponent>,
    private fb: FormBuilder,
    private outletService: OutletService,
    @Inject(MAT_DIALOG_DATA) public data: any) {
      this.pgManagementForm = this.fb.group({
        name: [null, Validators.required],
        city: [null, Validators.required],
        address: [null, Validators.required],
        group: [null, Validators.required],
        target: [null, Validators.pattern('\\d*')],
        latitude: [null, [Validators.required, Validators.pattern('-?\\d+(\\.\\d+)?')]],
        longitude: [null, [Validators.required, Validators.pattern('-?\\d+(\\.\\d+)?')]],
        region: [null, []],
        district: [null, []],
        ward: [null, []]
      });
  }


  initOutlet() { 
    if (this.data.type === 'update') {
      this.loading = true;
      const dataUpdated = this.data.data;
      // this.outletService.getoutlet(this.data.id).subscribe((dataUpdated: any) => {
        if (dataUpdated) {
          let longitude = null;
          let latitude = null;
          if (dataUpdated.location) {
            [latitude, longitude] = dataUpdated.location.split('|');
          }
          // const regionObject = Regions.find(({value}) => value === res.region);
          this.pgManagementForm.controls['name'].setValue(dataUpdated.name);
          this.pgManagementForm.controls['city'].setValue(dataUpdated.city);
          this.pgManagementForm.controls['address'].setValue(dataUpdated.address);
          this.pgManagementForm.controls['group'].setValue(dataUpdated.group);
          this.pgManagementForm.controls['target'].setValue(dataUpdated.target);
          this.pgManagementForm.controls['latitude'].setValue(latitude);
          this.pgManagementForm.controls['longitude'].setValue(longitude);
          this.pgManagementForm.controls['region'].setValue(dataUpdated.region);
          this.pgManagementForm.controls['ward'].setValue(dataUpdated.ward);
          this.pgManagementForm.controls['district'].setValue(dataUpdated.district);
        }
        this.loading = false;
      // }, _ => this.loading = false);
    }
  }

  ngOnInit(): void {
    this.cities = Cities;
    this.groups = Regions;
    this.title = this.data.type === 'update' ? 'Update Location' : 'Create Location';
    this.initOutlet();
  }

  createPg() {
    if (this.pgManagementForm.valid) {
      this.loading = true;
      const value = {...this.pgManagementForm.value};
      value.location = `${value.latitude}|${value.longitude}`
      delete value.latitude;
      delete value.longitude;
      if (this.data.type === 'create') {
        this.outletService.createoutlet(value).subscribe(res => {
          this.loading = false;
          this.dialogRef.close(res);
        }, _ => this.loading = false);
      }
      if (this.data.type === 'update') {
        this.outletService.updateoutlet(this.data.data.id, value).subscribe(res => {
          this.loading = false;
          this.dialogRef.close(res);
        }, _ => this.loading = false);
      }
    }
  }

}
