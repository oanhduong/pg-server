import {
  AfterViewInit,
  Component, Inject, OnDestroy, OnInit, ViewChild,
} from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { DeleteDialogComponent } from '../common/delete-dialog/delete-dialog.component';
import { ExportService } from 'src/app/services/export.service';
import { CompetitorsCreateDialogComponent } from './competitors-create-dialog/competitors-create-dialog.component';
import { forkJoin, of, Subscription } from 'rxjs';
import { CompetitorService } from 'src/app/services/competitor.service';
import { OutletService } from 'src/app/services/outlet.service';
import { switchMap } from 'rxjs/operators';
import { DatePipe } from '@angular/common';
import { CompetitorsModifyDialogComponent } from './competitors-modify-dialog/competitors-modify-dialog.component';


@Component({
  selector: 'app-competitors',
  templateUrl: './competitors.component.html',
  styleUrls: ['./competitors.component.scss'],
})
export class CompetitorsComponent implements OnInit, AfterViewInit, OnDestroy {
  displayedColumns: string[] =   ['startDate', 'outlet', 'user', 'images', 'program', 'action'];
  hidePagination = false;
  pageSizeOptions = [5, 10, 25, 100];
  loadSubscription: Subscription;
  // cities = [];
  // regions = [];
  filter: any = {
    page: 0,
    pageSize: this.pageSizeOptions[0],
    startDate_gte: null,
    endDate_lt: null
  };
  totalCount = 0;
  loading = false;

  exporting = false;
  dataNormal: any[] = [];

  filterTimeout;
  
  dataSource = new MatTableDataSource([]);


  @ViewChild(MatSort, { static: true }) sort: MatSort;


  constructor(public dialog: MatDialog, private exportService: ExportService,
    private competitorService: CompetitorService, private outletService: OutletService,
    private datePipe: DatePipe) { }
   
    ngOnDestroy(): void {
      if(this.loadSubscription) {
        this.loadSubscription.unsubscribe();
      }
    }
    
    ngAfterViewInit() {
      this.dataSource.sort = this.sort;
    }
    ngOnInit(): void {
      this.init();
    }
   
  init() {
    this.loading = true;
    if (this.loadSubscription) {
      this.loadSubscription.unsubscribe();
    }
    this.loadSubscription = forkJoin([
      this.competitorService.fetch(this.filter),
      this.competitorService.getTotalRecords(this.filter)
    ]).subscribe(([checkins, total]) => {
      this.mapData(checkins);
      this.totalCount = total;
      this.loading = false;
    }, _ => {
      this.loading = false;
    })
  }
  

  viewImage(images, title) {
    const dialogRef = this.dialog.open(CompetitorsCreateDialogComponent, {
      data: {
        images: images.split(', '),
        title: title
      },
      width: '100%',
      height: '60%'
    });

    dialogRef.afterClosed().subscribe(result => {
    });
    //   images = images.split(', ');
    //   console.log('images >>>>', images)
    //   images.forEach(async function(url, index){
    //     await new Promise<void>((resolve) => {
    //       setTimeout(() => {
    //         let link     = document.createElement('a');
    //         link.href    = url;
    //         link.target  = '_blank';
    //         link.id = index;
    //         link.click();
    //         return resolve();
    //       }, 10)
    //     })
    // });
  }

  export() {
    this.exporting = true;
    const fileName = 'competitor-reports';
    const header: any = {
      startDate: 'Start Date',
      endDate: 'End Date',
      outlet: 'OutletName',
      user: 'NNA Name',
      images: 'Photo',
      program: 'Program',
      note: 'Note',
    }
    let data = [header];
    if (this.hidePagination) {
      data = data.concat(this.dataNormal);
      this.exportService.exportExcel(data, fileName, true, [fileName]);
      this.exporting = false;
      return;
    } else {
      this.exporting = true;
      let newFilter = {...this.filter};
      delete newFilter.page;
      delete newFilter.pageSize;
      this.competitorService.fetch(newFilter)
        .subscribe((rs) => {
        let dataExport = this.mappingDataForExport(rs);
        data = data.concat(dataExport);
        this.exportService.exportExcel(data, fileName, true, [fileName]);
        this.exporting = false;
      }, _ => {
        this.exporting = false;
      })
    }
  }

  onPageChange(event) {
    this.filter.page = event.pageIndex;
    this.filter.pageSize = event.pageSize;
    this.init();
  }

  mappingDataForExport(list) {
    const data: any = list.map((element) => {
      const mapped = {
        program: element.program,
        startDate: element.startDate ? this.datePipe.transform(element.startDate, 'dd/MM/yyyy') : '',
        endDate: element.endDate ? this.datePipe.transform(element.endDate, 'dd/MM/yyyy') : '',
        note: element.note,
        user: element['pg_user'] ? element['pg_user'].name || element['pg_user'].username : '',
        outlet: element['pg_outlet'] ? element['pg_outlet'].name : '',
        images: (element.images || []).map(({url}) => `${url}`).join(', ')
      }
      return mapped
    })
    
    return data;
  }

  mappingDataAndDontSet(list) {
    const data: any = list.map((element) => {
      const mapped = {
        ...element,
        program_: element.program,
        startDate_: element.startDate ? this.datePipe.transform(element.startDate, 'dd/MM/yyyy') : '',
        endDate_: element.endDate ? this.datePipe.transform(element.endDate, 'dd/MM/yyyy') : '',
        note: element.note,
        user: element['pg_user'] ? element['pg_user'].name || element['pg_user'].username : '',
        outlet: element['pg_outlet'] ? element['pg_outlet'].name : '',
        // region: element['pg_outlet'] ? element['pg_outlet'].group : '',
        // city: element['pg_outlet'] ? element['pg_outlet'].city : '',
        images_: (element.images || []).map(({url}) => `${url}`).join(', ')
      }
      return mapped
    })
    
    return data;
  }

  mapData(list = []) {
    const data: any = this.mappingDataAndDontSet(list);

    this.dataNormal = this.mappingDataForExport(list);
    this.dataSource = new MatTableDataSource(data);
    this.dataSource.sort = this.sort;
  }

  onFilterData({region, city, name, startDate, endDate}) {
    if (endDate) {
      endDate._d.setDate(endDate._d.getDate() + 1);
    }
    if (this.filterTimeout) {
      clearTimeout(this.filterTimeout);
    }
    if (region || city || name) {
      this.hidePagination = true;
      let newFilter = {
        group: region, city, name
      }
      this.filterTimeout = setTimeout(() => {
        this.loading = true;
        this.outletService.fetchoutlets(newFilter).pipe(switchMap((rs) => {
          let listCheckins = [];
          (rs || []).map(({pg_competitors}) => {
            listCheckins = listCheckins.concat(pg_competitors);
          })
          return of(listCheckins);
        })).pipe(switchMap((rs) => {
          let ids = rs.filter(({id}) => id).map(({id}) => id);
          const filterDate = {
            startDate_gte: startDate || null,
            endDate_lt: endDate || null
          }
          return this.competitorService.fetchByIds(ids, filterDate)
        })).subscribe((rs) => {
          this.mapData(rs);
          this.loading = false;
        })
      }, 100)
    } else {
      this.hidePagination = false;
      this.loading = true;
      this.filter.page = 0;
      this.filterTimeout = setTimeout(() => {
          this.filter.startDate_gte = startDate|| null;
          this.filter.endDate_lt = endDate || null;
          this.filter.page = 0;
          this.filter.pageSize = this.pageSizeOptions[0];
          this.init();
      }, 100)
    }
  }

  modify(element) {
    const dialogRef = this.dialog.open(CompetitorsModifyDialogComponent, {
      data: element,
      width: '50%',
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.init();
      }
    });
  }

  deleteItem({id}) {
    const dialogRef = this.dialog.open(DeleteDialogComponent, {
      data: {
        header: 'Delete Competitor',
        title: 'Do you want to delete this competitor?'
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.competitorService.delete(id).subscribe(res => {
          this.filter.page = 0;
          this.filter.pageSize = this.pageSizeOptions[0];
          this.init();
        });
      }
    });
  }
}

