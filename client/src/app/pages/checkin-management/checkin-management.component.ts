import { DatePipe } from '@angular/common';
import {
  AfterViewInit,
  Component, OnDestroy, OnInit, ViewChild,
} from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { forkJoin, of, Subscription } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { CheckinService } from 'src/app/services/checkin.server';
import { ExportService } from 'src/app/services/export.service';
import { OutletService } from 'src/app/services/outlet.service';
import { DeleteDialogComponent } from '../common/delete-dialog/delete-dialog.component';
import { CheckinAddressDialogComponent } from './checkin-address-dialog/checkin-address-dialog.component';
import { CheckinCreateDialogComponent } from './checkin-create-dialog/checkin-create-dialog.component';

@Component({
  selector: 'app-checkin-management',
  templateUrl: './checkin-management.component.html',
  styleUrls: ['./checkin-management.component.scss'],
})

export class CheckinManagementComponent implements OnInit, AfterViewInit, OnDestroy {
  displayedColumns: string[] = ['username', 'outlet', 'startDate', 'startHour', 'endDate', 'endHour', 'workingTime', 'outletImage', 'userImage', 'address' ];
  hidePagination = false;
  pageSizeOptions = [5, 10, 25, 100];
  loadingSubscription: Subscription;
  filter: any = {
    page: 0,
    pageSize: this.pageSizeOptions[0],
    startDate_gte: null,
    endDate_lt: null
  };
  totalCount = 0;
  loading = false;
  zoom = 15;

  filterTimeout;
  
  dataSource = new MatTableDataSource([]);
  dataNormal: any[] = [];
  exporting = false;


  @ViewChild(MatSort, { static: true }) sort: MatSort;


  constructor(public dialog: MatDialog, private exportService: ExportService,
    private checkinService: CheckinService, private outletService: OutletService,
    private datePipe: DatePipe) { }
    
    ngOnDestroy(): void {
      if (this.loadingSubscription) {
        this.loadingSubscription.unsubscribe();
      }
    }
    
    ngAfterViewInit() {
      this.dataSource.sort = this.sort;
    }
    ngOnInit(): void {
      this.init();
    }
   
  init() {
    this.loading = true;
    if (this.loadingSubscription) {
      this.loadingSubscription.unsubscribe();
    }
    this.loadingSubscription = forkJoin([
      this.checkinService.fetch(this.filter),
      this.checkinService.getTotalRecords(this.filter)
    ]).subscribe(([checkins, total]) => {
      this.mapData(checkins);
      this.totalCount = total;
      this.loading = false;
    }, _ => {
      this.loading = false;
    })
  }
  createPg() {
    const dialogRef = this.dialog.open(CheckinCreateDialogComponent, {

    });

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }

  viewImage(url, title) {
    const dialogRef = this.dialog.open(CheckinCreateDialogComponent, {
      data: {
        url: url,
        title: title
      },
      width: '50%'
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }

  deletePg() {
    const dialogRef = this.dialog.open(DeleteDialogComponent, {
      data: {
        header: 'Xóa PG',
        title: 'Bạn có muốn xóa PG này không?'
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }

  openMap(address) {
    const dialogRef = this.dialog.open(CheckinAddressDialogComponent, {
      data: { address: address }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }

  export() {
    this.exporting = true;
    const fileName = 'checkin';
    const header: any = { 
      username: 'NNA/User', 
      outlet: 'Location/Store', 
      startDate: 'Checkin Date', 
      startHour: 'Checkin Time', 
      endDate: 'Checkout Date',
      endHour: 'Checkout Time',
      workingSheet: 'Working Time',
      outletImage: 'Outlet Photo', 
      userImage: 'NNA/User Photo',
      address: 'Location'
    };
    let data = [header];
    if (this.hidePagination) {
      data = data.concat(this.dataNormal);
      this.exportService.exportExcel(data, fileName, true, [fileName]);
      this.exporting = false;
      return;
    } else {
      this.exporting = true;
      let newFilter = {...this.filter};
      delete newFilter.page;
      delete newFilter.pageSize;
      if (this.loadingSubscription) {
        this.loadingSubscription.unsubscribe();
      }
      this.loadingSubscription = this.checkinService.fetch(newFilter)
        .subscribe((rs) => {
        let dataExport = this.mappingDataAndDontSet(rs);
        data = data.concat(dataExport);
        this.exportService.exportExcel(data, fileName, true, [fileName]);
        this.exporting = false;
      }, _ => {
        this.exporting = false;
      })
    }
  }

  onPageChange(event) {
    this.filter.page = event.pageIndex;
    this.filter.pageSize = event.pageSize;
    this.init();
  }

  mappingDataAndDontSet(list) {
    const data: any = list.map((checkin) => {
      const mapped = {
        username: checkin['pg_user'] ? checkin['pg_user'].username : '',
        address: `https://maps.google.com/maps?q=${checkin.address.replace('|', ',')}`,
        startDate: checkin.startDate ? this.datePipe.transform(checkin.startDate, 'dd/MM/yyyy') : '',
        endDate: checkin.endDate ? this.datePipe.transform(checkin.endDate, 'dd/MM/yyyy') : '',
        // user: checkin['pg_user'] ? checkin['pg_user'].name: '',
        outlet: checkin['pg_outlet'] ? checkin['pg_outlet'].name : '',
        outletImage: (checkin.images || [])[1] ? `${(checkin.images || [])[1].url}` : null,
        userImage: (checkin.images || [])[0] ? `${(checkin.images || [])[0].url}` : null,
        startHour: checkin.startDate ? this.datePipe.transform(checkin.startDate, 'HH:mm') : '',
        endHour: checkin.endDate ? this.datePipe.transform(checkin.endDate, 'HH:mm') : '',
        workingSheet: this.convertTime(checkin.startDate, checkin.endDate)
      }
      return mapped
    })
    return data;
  }

  convertTime(startDate, endDate) {
    const timeStamp = new Date(endDate).getTime() - new Date(startDate).getTime()
    const hour = Math.floor(timeStamp / (60*60*1000));
    const minute = Math.floor((timeStamp - (hour * 60 * 60 * 1000)) / (60*1000));
    const second = timeStamp - (hour * 60 * 60 * 1000) - (minute * 60 * 1000);
    return `${hour}h:${('0' + minute).slice(-2)}m:${('0' + second).slice(-2)}s`
  }

  mapData(checkins = []) {
    const data: any = this.mappingDataAndDontSet(checkins);
    this.dataNormal = data;
    this.dataSource = new MatTableDataSource(data);
    this.dataSource.sort = this.sort;
  }

  onFilterData({region, city, name, startDate, endDate}) {
    if (this.filterTimeout) {
      clearTimeout(this.filterTimeout);
    }
    if (region || city || name) {
      this.hidePagination = true;
      let newFilter = {
        group: region, city, name
      }
      this.filterTimeout = setTimeout(() => {
        this.loading = true;
        this.outletService.fetchoutlets(newFilter).pipe(switchMap((rs) => {
          let listCheckins = [];
          (rs || []).map(({pg_checkins}) => {
            listCheckins = listCheckins.concat(pg_checkins);
          })
          return of(listCheckins);
        })).pipe(switchMap((rs) => {
          let ids = rs.filter(({id}) => id).map(({id}) => id);
          const filterDate = {
            startDate_gte: startDate || null,
            endDate_lt: endDate || null
          }
          return this.checkinService.fetchByIds(ids, filterDate)
        })).subscribe((rs) => {
          this.mapData(rs);
          this.loading = false;
        })
      }, 100)
    } else {
      this.hidePagination = false;
      this.loading = true;
      this.filter.page = 0;
      this.filterTimeout = setTimeout(() => {
          this.filter.startDate_gte = startDate || null;
          this.filter.endDate_lt = endDate || null;
          this.filter.page = 0;
          this.filter.pageSize = this.pageSizeOptions[0];
          this.init();
      }, 100)
    }
  }

}

