import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
@Component({
  selector: 'app-checkin-create-dialog',
  templateUrl: './checkin-create-dialog.component.html',
  styleUrls: ['./checkin-create-dialog.component.scss'],
})
export class CheckinCreateDialogComponent {
  imageUrl;
  title;
  constructor(@Inject(MAT_DIALOG_DATA) public data: any) {
    this.imageUrl = data.url;
    this.title = data.title;
  }

  createPg() {
    console.log('ahihi');
  }
}
