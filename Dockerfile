# Use an official Node runtime as a parent image
FROM node:12.7.0-alpine

WORKDIR /app
COPY client /app/client
COPY server /app
WORKDIR /app/client
RUN npm install
RUN npm run build

WORKDIR /app
RUN cp -R /app/client/dist/* /app/public
# RUN rm -rf server
# RUN rm -rf client
RUN npm install

ENV PORT=3000
ENV STRAPI_TOKEN=
ENV TZ=Asia/Ho_Chi_Minh

EXPOSE $PORT
CMD ["node", "server.js"]
